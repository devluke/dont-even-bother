from pathlib import Path
import json

import config

DEFAULT_DATA = {
    'status': None,
    'gender': 'm'
}


async def get_path(user_id):
    path = Path(config.SAVE_CONFIG['directory'], f'{user_id}.json')
    if not path.exists():
        path.write_text(json.dumps(DEFAULT_DATA, separators=(',', ':')))
    return path


async def set_value(user_id, field, value):
    path = await get_path(user_id)
    data = json.loads(path.read_text())
    data[field] = value
    path.write_text(json.dumps(data, separators=(',', ':')))


async def get_value(user_id, field):
    path = await get_path(user_id)
    data = json.loads(path.read_text())
    return data[field]
