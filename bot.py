import re

from discord.errors import Forbidden
import discord

import config
import save

client = discord.Client()


@client.event
async def on_ready():
    guilds = client.guilds
    print(f'Member of {len(guilds)} guilds:')
    for g in guilds:
        print(f'    {g} ({g.member_count})')
    print(f'Logged in as {client.user}')


@client.event
async def on_message(msg):
    if msg.author == client.user:
        return

    is_mentioned = msg.content.startswith(client.user.mention)
    is_dm = isinstance(msg.channel, discord.DMChannel)
    if is_mentioned or is_dm:
        new_msg = msg.content.replace(f'{client.user.mention} ', '', 1).split()
        new_msg = ' '.join(new_msg)
        lower_msg = new_msg.lower()
        success = False
        self_mention_test = f'{msg.author.mention} is '.lower()
        self_mention_dm_test = f'@{msg.author} is '.lower()
        if lower_msg.startswith((self_mention_test, self_mention_dm_test)):
            first_is = lower_msg.find('is')
            status = new_msg[first_is + 3:]
            await save.set_value(msg.author.id, 'status', status)
            success = True
        elif lower_msg in ('never mind', 'nevermind', 'nvm'):
            await save.set_value(msg.author.id, 'status', None)
            success = True
        elif lower_msg == 'gender':
            try:
                await msg.add_reaction('👨')
                await msg.add_reaction('👩')
            except Forbidden:
                pass
        if success:
            try:
                await msg.add_reaction('👍')
            except Forbidden:
                pass
    else:
        for m in msg.mentions:
            status = await save.get_value(m.id, 'status')
            gender = await save.get_value(m.id, 'gender')
            pronouns = {
                'm': 'he',
                'f': 'she'
            }
            if status is not None:
                try:
                    await msg.channel.send(f"""{msg.author.mention}, don't \
even bother mentioning **{m}**, {pronouns[gender]} is {status}""")
                except Forbidden:
                    pass


@client.event
async def on_reaction_add(reaction, user):
    first_user = (await reaction.users().flatten())[0]
    if user == client.user or first_user != client.user:
        return

    genders = {
        '👨': 'm',
        '👩': 'f'
    }
    if reaction.emoji in genders.keys():
        await save.set_value(user.id, 'gender', genders[reaction.emoji])
        try:
            for g in genders.keys():
                await reaction.message.remove_reaction(g, client.user)
                await reaction.message.remove_reaction(g, user)
            await reaction.message.add_reaction('👍')
        except Forbidden:
            pass

    emoji_order = ('👍', '👌', '👎', '🖕', '😟🔫')
    for e in emoji_order:
        if e == reaction.emoji:
            next_emoji = emoji_order[emoji_order.index(e) + 1]
            for e2 in list(next_emoji):
                try:
                    await reaction.message.add_reaction(e2)
                except Forbidden:
                    pass
            break

client.run(config.DISCORD_CONFIG['token'])
